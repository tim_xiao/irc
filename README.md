# Incremental Risk Charge Calculation

The Basel Committee on Banking Supervision (see Basel [2009 a]) released the new guidelines for Incremental Risk Charge (IRC) that are part of the new rules developed in response to the financial crisis and is a key part of a series of regulatory enhancements being rolled out by regulators.

IRC supplements existing Value-at-Risk (VaR) and captures the loss due to default and migration events at a 99.9% confidence level over a one-year capital horizon. The liquidity of position is explicitly modeled in IRC through liquidity horizon and constant level of risk (see Xiao[2017]).

The constant level of risk assumption in IRC reflects the view that securities and derivatives held in the trading book are generally more liquid than those in the banking book and may be rebalanced more frequently than once a year (see Aimone [2018]).  IRC should assume a constant level of risk over a one-year capital horizon which may contain shorter liquidity horizons. This constant level of risk assumption implies that a bank would rebalance, or rollover, its positions over the one-year capital horizon in a manner that maintains the initial risk level, as indicated by the profile of exposure by credit rating and concentration.

The current market risk capital rule is:

Total market risk capital = general market risk capital 
     + basic specific risk capital				(1)
     + specific risk surcharge
where
	General market risk capital = 3 x  
	Basic specific risk capital = 3 x  
	Specific risk surcharge = (m – 3) x  
where m is the specific risk capital multiplier under regulators’ guidance

The new market risk capital standard will be:
	  Total market risk capital = general market risk capital 
+ basic specific risk capital				(2)
        + incremental risk charge
where Incremental risk charge =  

In this paper, we present a methodology for calculating IRC. First, a Merton-type model is introduced for simulating default and migration. The model is modified to incorporate concentration. The calibration is also elaborated. Second, a simple approach to determine market data, including equity, in response to default and credit migration is presented. Next, a methodology toward constant level of risk is described. The details of applying the constant level of risk assumption and aggregating different subportfolios are addressed. Finally, the empirical and numerical results are presented.

References 

Alessandro Aimone, 2018, “ING’s market risk charge edges higher,” Risk Quantum, 2018

Basel Committee on Banking Supervision, 31 July 2003, “The new Basel capital accord.”

Basel Committee on Banking Supervision, July 2008, “Guidelines for Computing Capital for Incremental Default Risk in the Trading Book.”

Basel Committee on Banking Supervision, July 2009 (a), “Guidelines for Computing Capital for Incremental Risk in the Trading Book.”

Basel Committee on Banking Supervision, July, 2009 (b), “Revisions to the Basel II market risk framework.” 

Basel Committee on Banking Supervision, October 2009 ©, “Analysis of the trading book quantitative impact study.”

Gary Dunn, April 2008, “A multiple period Gaussian Jump to Default Risk Model.”

FinPricing, Market Data Solution, https://finpricing.com/lib/IrCurveIntroduction.html

Erik Heitfield, 2003, “Dealing with double default under Basel II,” Board of Governors of the Federal Reserve System.

Jongwoo Kim, Feb 2009, “Hypothesis Test of Default Correlation and Application to Specific Risk,” RiskMetrics Group.

J.P.Morgan, April, 1997, “CreditMetrics – Technical Document.” 

Dirk Tasche, Feb 17, 2004, “The single risk factor approach to capital charges in case of correlated loss given default rates.” 

Tim Xiao, 2017, “A New Model for Pricing Collateralized OTC Derivatives.” Journal of Derivatives, 24(4), 8-20.


